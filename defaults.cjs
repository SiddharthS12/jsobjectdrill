const defaults = (obj, defaultProps) => {
    let newObj = {}
    for (let key in obj) {
        if (obj[key] === undefined) {
            newObj[key] = defaultProps[key]
        } else {
            newObj[key] = obj[key]
        }
    }
    return newObj
}

module.exports = defaults