const mapObject = (obj, cb) => {
    let newObj = {}
    for (let keys in obj) {
        newObj[keys] = cb(keys, obj[keys])
    }
    return newObj;
}

module.exports = mapObject