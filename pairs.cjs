const pairs = (obj) => {
    let pairArr = [];
    for (let key in obj) {
        pairArr.push([key, obj[key]])
    }
    return pairArr
}

module.exports = pairs