const  defaults  = require("../defaults");

const testObject = {
  name: "Bruce Wayne",
  age: 36,
  location: "Gotham",
  superHero: undefined,
  power: undefined,
  movie: undefined,
};

const defaultProps = { superHero: "Batman", power: "Infinite Money" };

const ans = defaults(testObject, defaultProps);

console.log(ans);
